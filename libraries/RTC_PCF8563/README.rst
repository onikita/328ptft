
This is my fixed/expanded version of the original Rtc_Pcf8563 library for Arduino.
All credits go to the original authors.

The main difference here is the addition of a few 12 hour modes and meridiem(AM/PM designations).
Available 12 hour modes:
						Format
RTCC_TIME_12HM			01:25
RTCC_TIME_12HMM		   01:25PM
RTCC_TIME_12HMSM	  01:25:30PM

Available 24hour modes:

RTCC_TIME_HM		    13:25
RTCC_TIME_HMS		  13:25:30

Using 12 hour modes: 
	
	Serial.print(rtc.formatTime(RTCC_TIME_12HMM));


Using meridiem seperately:
	
	if(rtc.getMeridiem == 1) {
		Serial.print("PM");
	} else {
		Serial.print("AM");
	}
	
	const char* a[3] = {"AM", "PM"}
	bool amPM = rtc.getMeridiem();
	Serial.print(a[amPM]);
	
	
