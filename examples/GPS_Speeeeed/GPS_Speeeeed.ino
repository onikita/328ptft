#include <TFT.h>
#include <SPI.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "PCF8574.h"

TFT tft = TFT(TFT_CS, TFT_DC, TFT_RST);
PCF8574 pcf(0x20);
TinyGPSPlus gps;
SoftwareSerial ss(6, 7);

bool satFlag = true;

void setup() {
  pinMode(5, OUTPUT);
  tft.begin();
  pcf.begin();
  ss.begin(9600);
  tft.fillScreen(BLACK);
  tft.setTextColor(POWDER, BLACK);
  analogWrite(5, 155);
}

void dispInit() {
  //tft.fillScreen(BLACK);
  tft.setTextSize(2);
  tft.stroke(NUKE);
  tft.text("Lat:", 13, 10);
  tft.text("Long:", 93, 10);
  tft.text("Alt:", 43, 55);
  tft.text("MPH:", 93, 55);
  tft.stroke(DGRAY);
  tft.line(80, 0, 80, 128); //vertical line
  tft.line(0, 52, 160, 52); //horizontal line
  tft.setTextSize(1);
  tft.stroke(POWDER);
  tft.setTextColor(POWDER, BLACK);

}

void noSat() {
}

void loop() {
  while (ss.available() > 0) {
    gps.encode(ss.read());
  }

  if (gps.satellites.isUpdated()) {
    //display no sat message once after connections are lost
    if ((gps.satellites.value() == 0) && (satFlag == true)) {
      tft.setCursor(1, 0);
      tft.print("No Satellites");
      satFlag = false;
    } else if ((satFlag == false) && (gps.satellites.value() > 0)) {
      //once connections are established or regained
      satFlag = true;
      dispInit(); //clear display and change to main screen
      tft.setCursor(1, 0);
      tft.print("GPS Demo       ");
      tft.setCursor(110, 0);
      tft.print(gps.satellites.value());
    } else {
      //update satellite number
      tft.setCursor(110, 0);
      tft.print(gps.satellites.value());
    }
  }

  if (satFlag == true) {
    if (gps.location.isUpdated()) {
      tft.setCursor(10, 38);
      tft.print(gps.location.lat(), 6);
      tft.setCursor(90, 38);
      tft.print(gps.location.lng(), 6);
    }
    if (gps.speed.isUpdated()) {
      tft.setCursor(90, 83);
      tft.print(gps.speed.mph());
    }
    if (gps.altitude.isUpdated()) {
      tft.setCursor(40, 83);
      tft.print(gps.altitude.feet());
    }
    if (gps.course.isUpdated()) {
      tft.setCursor(80 - (sizeof(gps.course.value()) * 3), 118);
      tft.print(gps.course.value());
    }
  }
  /*
    if ((gps.charsProcessed()) < 10 && (millis() > 5000)) {
    tft.setCursor(2, 70);
    tft.print("ERROR - No Device Detected");
    } else {
    tft.setCursor(2, 70);
    tft.print("                            ");
    }
  */
}
