#include <SPI.h>
#include "RF24.h"
#include <Wire.h>
#include <TFT.h>
#include "PCF8574.h"
#include <Rtc_Pcf8563.h>

String message;
char prep[27];
char* messagesOut[4] = {" ", " ", " "};
char* messagesIn[4] = {"the", "Fox", "Jumps"};
int letterNum = 97;
int charNum = 1;
int button;
int mIn = 0;
int mOut = 0;
int yPos;
int mode = 0;

PCF8574 pcf(0x20);//intialize PCF8574 at i2c address
TFT tft = TFT(10, 9, 8);
RF24 nrf(6, 7); //initialize NRF24 -pins D6 & D7 for CE and CSN
byte addresses[][6] = {"0"};

struct package {
  char msg[27];
  int msglength;
};

typedef struct package Package;
Package dataout;
Package datain;

void setup() {
  pinMode(5, OUTPUT);
  tft.begin();
  pcf.begin();
  tft.setRotation(3);
  tft.fillScreen(BLACK);
  tft.setTextColor(FAV, BLACK);
  tft.textSize(2);
  tft.setCursor(18, 1);
  tft.print("NRF24 Text");
  analogWrite(5, 190);
  tft.textSize(1);
  tft.setTextColor(DGRAY, BLACK);
  tft.setCursor(0, 100);
  nrf.begin();
  nrf.setChannel(115);
  nrf.setPALevel(RF24_PA_LOW);
  nrf.setDataRate(RF24_250KBPS);
  nrf.openReadingPipe(1, addresses[0]);
  nrf.startListening();
}

void sendMessage() {
  nrf.stopListening();
  message.toCharArray(dataout.msg, 28);
  nrf.openWritingPipe(addresses[0]);
  nrf.write(&dataout, sizeof(dataout));
  nrf.openReadingPipe(1, addresses[0]);
  nrf.startListening();
  //the following lines are to clear the previous outgoing message from the screen

  message = " ";
  for (int a = 0; a < 28; a++) {
    message = String(message + " ");
  }
  tft.setCursor(0, 100);
  tft.print(message);
  message.trim();
}

void getMessage() {
  if (nrf.available()) {
    tone(A3, 722, 335);
    tone(A3, 650, 137);
    tone(A3, 1100, 155);

    while (nrf.available()) {
      nrf.read(&datain, sizeof(datain));
    }
    /*
      messagesIn[2] = messagesIn[1];
      messagesIn[1] = messagesIn[0];
      messagesIn[0] = datain.msg;
      tft.setCursor(0, 18);
      yPos = 18;
      for(int d = 0; d < 4; d++){
      tft.setCursor(0, yPos);
      tft.print(messagesIn[d]);
      yPos += 10;
      }
    */
    tft.setCursor(0, 50);
    tft.print(".");
  } else {
    tft.setCursor(0, 50);
    tft.print("!");
  }
}

void loop() {
  for (int i = 0; i < 8; i++) {
    if (pcf.readButton(i) != 1) {
      button = i;
      pcf.write(i, 1);
    }
  }
  if (button == 5) {//up
    letterNum += 1;
    if (letterNum > 122) {
      letterNum = 32;
    }
    if ((letterNum > 32) && (letterNum < 48)) letterNum = 48;
    if ((letterNum > 57) && (letterNum < 97)) letterNum = 97;
  }
  if (button == 3) {//down
    letterNum -= 1;
    if (letterNum < 32) {
      letterNum = 122;
    }
    if ((letterNum < 97) && (letterNum > 57)) letterNum = 57;
    if ((letterNum < 48) && (letterNum > 32)) letterNum = 32;
  }
  if (button == 1) {//move cursor right/add previous character to message string
    if (message.length() < 27) {
      message = String(message + char(letterNum));
    }
    letterNum = 97;
    tft.setCursor(0, 100);
    tft.print(message);
  }
  tft.setCursor(message.length() * 7, 100);
  tft.print(char(letterNum));

  if (button == 2) { // move cursor left
    tft.print(" ");
    if (message.length() > 0) message.remove(message.length() - 1);
    letterNum = 97;
  }
  if (button == 6) { //send message on center button press
    sendMessage();
  }

  button = 0;
  getMessage(); //check for incomming message
  delay(100);
}
