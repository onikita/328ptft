#include <Wire.h>
#include "PCF8574.h"
#include <Rtc_Pcf8563.h>
#include <SPI.h>
#include <TFT.h>

const char* days[8] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
int wkday;
int mins;
int hr;
int d;
int y;
int m;
int mode = 0;
int button = 0;

PCF8574 pcf(0x20);
TFT tft = TFT(10, 9, 8);//pin 12 for v1 328PTFT, pin 8 for v2 and up
Rtc_Pcf8563 rtc;

void upDate() {
  tft.setTextSize(2);
 
  tft.setTextColor(DRAB, BG);
   tft.setCursor(16, 80);
  //date format options: RTCC_DATE_US, RTCC_DATE_WORLD, and RTCC_DATE_ASIA
  tft.print(rtc.formatDate(RTCC_DATE_US)); //initialize display with date
  
  wkday = rtc.getWeekday();
  if ((wkday == 0) || (wkday == 6)) {
    tft.setTextColor(WHITE, BG);
  }
  tft.setCursor(24, 110);
  tft.print(days[wkday]); //initialize display with weekday
  tft.setTextColor(DRAB, BG);

  tft.setTextSize(3);
  mode = 0;
  delay(450);
}

void setup()
{
  tft.begin();
  pcf.begin();

  //rtc.initClock();
  //date, weekday, month, century(1=1900, 0=2000), year(0-99)
  //rtc.setDate(25, 4, 4, 0, 19);
  //hr, min, sec
  //rtc.setTime(16, 50, 32);
  //both format functions call the internal getTime() so that the
  //formatted strings are at the current time/date.
  pinMode(5, OUTPUT);
  tft.setRotation(3);
  tft.fillScreen(BG);
  tft.setTextSize(2);
  tft.setTextColor(POWDER);
  tft.setCursor(8, 0);
  tft.print("PCF8563 Demo");
  analogWrite(5, 185);
  upDate();
}

void loop(){ 
  while (mode == 0) {
    
    tft.setCursor(8, 35);
    tft.print(rtc.formatTime(RTCC_TIME_HMS));
    if (wkday != rtc.getWeekday()) {
      upDate();
    }
    //Button detection
    for (int i = 0; i < 8; i++) {
      if (pcf.readButton(i) != 1) {
        button = i;
        pcf.write(i, 1);
        tft.setCursor(120, 120);
        tft.print(button);
        if (button == 6) {
          //store all current time values as variables for manipulation
          y = rtc.getYear();
          d = rtc.getDay();
          m = rtc.getMonth();
          hr = rtc.getHour();
          mins = rtc.getMinute();
          wkday = rtc.getWeekday();
          button = 0;
          tone(A3, 850, 190);
          delay(100);
          mode = 1; //switch from run mode to set mode
        }
      }
    }
    delay(900);
  }
   while (mode != 0) {
    for (int i = 0; i < 8; i++) {
      if (pcf.readButton(i) != 1) {
        button = i;
      }
    }
    if (button == 3) { //adjust down
      if (mode == 1) {
        hr -= 1;
        if (hr < 0) hr == 23;
      } else if (mode == 2) {
        mins -= 1;
        if (mins < 0) mins = 59;
      } else if (mode == 3) {
        m -= 1;
        if (m < 1) m = 12;
      } else if (mode == 4) {
        d -= 1;
        if (d < 1) d = 31;
      } else if (mode == 5) {
        y -= 1;
        if (y < 10) y = 10;
      } else if (mode == 6) {
        wkday -= 1;
        if (wkday < 0) wkday == 6;
      }
    }


    if (button == 5) { //adjust selection up
      if (mode == 1) {
        hr += 1;
        if (hr >= 24) hr = 0;
      } else if (mode == 2) {
        mins += 1;
        if (mins == 60) mins = 0;
      } else if (mode == 3) {
        m += 1;
        if (m > 12) m = 1;
      } else if (mode == 4) {
        d += 1;
        if (d > 31) d = 1;
      } else if (mode == 5) {
        y += 1;
        if (y > 30) y = 19;
      } else if (mode == 6) {
        wkday += 1;
        if (wkday > 6) wkday = 0;
      }
    }
    tft.setTextColor(RED, BG);
    rtc.setDate(d, wkday, m, 0, y);
    tft.setCursor(16, 80);
    tft.setTextSize(2);
    tft.print(rtc.formatDate(RTCC_DATE_US));
    tft.setCursor(24,110);
    tft.print(days[wkday]);
    tft.setCursor(8, 35);
    tft.setTextSize(3);
    rtc.setTime(hr, mins, 00);
    tft.print(rtc.formatTime(RTCC_TIME_HMS));
    tft.setCursor(120,120); //debugging
    tft.print(mode);
    if ((button == 1) && (mode < 7)) {
      mode += 1;
    }

    if ((button == 2) && (mode > 1)) {
      mode -= 1;
    }

    if(button == 6) {
      button = 0;
      upDate();
    }
    button = 0;
  }

  
}
