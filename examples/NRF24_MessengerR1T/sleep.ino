void sleep(){   //preparing for sleepytime
  EEPROM.update(3, brightness);
  sleep_enable();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  //display BL off
  analogWrite(5, 0);
  //naptime
  sleep_cpu(); //anything beyond this point happens after an interrupt is triggered and the processor wakes
  //turn on backlight again
  analogWrite(5, brightness);
  //resume normal
}
