#include <Wire.h>
#include "RTClib.h"
#include <SPI.h>
#include <TFT.h>
#include "PCF8574.h"

TFT tft = TFT(10, 9, 12);
PCF8574 pcf(0x20);
PCF8563 rtc;

int button;
int mode = 0; //0 = normal 1 = set hour 2 = set min 3 = month 4 = set day 5 = set year 6 = update date
int hr;
int minutes;
int sec;
int m;
int d;
int y;

void dispInit() {
  DateTime now = rtc.now();

  tft.fillScreen(BG);
  tft.setTextColor(POWDER, BG);
  tft.setTextSize(2);
  tft.setCursor(35, 0);
  tft.print("RTC Demo");
  tft.setTextColor(TXT, BG);
  tft.setCursor(20, 80);

  if (now.month() < 10) {
    tft.print("0");
  }
  tft.print(now.month(), DEC);
  tft.print("/");
  if (now.day() < 10) {
    tft.print("0");
  }
  tft.print(now.day(), DEC);
  tft.print("/");
  tft.print(now.year(), DEC);

  tft.setTextSize(3);
}

void setup () {
  Serial.begin(57600);
  pcf.begin();
  tft.begin();
  tft.setRotation(3);
  analogWrite(5, 155);
  Wire.begin();
  rtc.begin();

  dispInit(); //initialize display

  if (!rtc.isrunning()) { //set date and time to compiled time if time isn't set
    rtc.adjust(DateTime(__DATE__, __TIME__));
  }
  // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  tft.setTextSize(3);
}

void loop () {
  while (mode == 0) { //normal operation loop
    DateTime now = rtc.now();
    tft.setCursor(8, 50);
    if (now.hour() < 10) {
      tft.print("0");
    }
    //    tft.print(now.hour(), DEC);
    tft.print(now.hour());
    tft.print(":");
    if (now.minute() < 10) {
      tft.print("0");
    }
    //    tft.print(now.minute(), DEC);
    tft.print(now.minute());
    tft.print(":");
    if (now.second() < 10) {
      tft.print("0");
    }
    //    tft.print(now.second(), DEC);
    tft.print(now.second());

    if ((now.hour() == 23) && (now.second() == 59)) {
      dispInit(); //print new date to display at change of day;
    }


    for (int i = 0; i < 8; i++) {
      if (pcf.readButton(i) != 1) {
        button = i;
        pcf.write(i, 1);
        // tft.setCursor(120, 0); //debugging
        // tft.print(button); //debugging
        if (button == 7) {
          y = now.year();
          d = now.day();
          m = now.month();
          hr = now.hour();
          minutes = now.minute();
          button = 0;
          mode = 1;
        }
      }
    }
    delay(920);
  }
  while (mode != 0) {//set time
    DateTime now = rtc.now();
    tft.setTextColor(RED, BG);
    for (int i = 0; i < 8; i++) {
      if (pcf.readButton(i) != 1) {
        button = i;
        pcf.write(i, 1);
      }
    }

    for (int i = 0; i < 8; i++) {
      if (pcf.readButton(i) != 1) {
        button = i;
      }
    }
    if ( button == 2) { //adjust down
      if (mode == 1) {
        hr -= 1;
        if (hr < 0) hr = 23;
      } else if (mode == 2) {
        minutes -= 1;
        if (minutes < 0) minutes = 59;
      } else if (mode == 3) {
        m -= 1;
        if(m < 1) m = 12;
      } else if (mode == 4) {
        d -= 1;
        if(d < 1) d = 31;
      } else if (mode == 5) {
        y -= 1;
        if(y < 2010) y = 2010;
      }
    }

    if (button == 4) { //adjust up
      if (mode == 1) {
        hr += 1;
        if (hr == 24) hr = 0; //roll hours back to 0 from 24
      } else if (mode == 2) {
        minutes += 1;
        if (minutes == 60) minutes = 0; //roll minutes back to 0 from 60
      } else if (mode == 3) {
        m += 1;
        if (m > 12) m = 1; //roll month back to 1 from 12
      } else if (mode == 4) {
        d += 1;
        if (d > 31) d = 1; // roll day back to 0 from 31
      } else if (mode == 5) {
        y += 1;
      }
    }

    rtc.adjust(DateTime(y, m, d, hr, minutes, 0));
    tft.setCursor(8, 50);//print time
    if (hr < 10) tft.print("0");
    tft.print(hr);
    tft.print(":");
    if (minutes < 10) tft.print("0");
    tft.print(minutes);
    tft.print(":");
    tft.print("00");
    
    tft.setTextSize(2);
    tft.setCursor(20, 80); //print date
    if (m < 10) tft.print("0");
    tft.print(m);
    tft.print("/");
    if (d < 10) tft.print("0");
    tft.print(d);
    tft.print("/");
    tft.print(y);

//  tft.setCursor(120, 0); //debugging
//  tft.print(mode); //debugging;
   
    if (button == 1) {
      mode += 1;
    }
    if (button == 5) {
      mode -= 1;
    }
    if (button == 7) {
      dispInit();
      delay(100);
      mode = 0;
    }

    button = 0; //reset button variable to 0 for next loop
    tft.setTextSize(3);//change text size back to 3 for printing the time next loop
  }
}
