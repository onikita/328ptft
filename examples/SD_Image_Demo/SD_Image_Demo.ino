/*
   With this example you will see how to make a simple slideshow using your 328PTFT board and a Micro SD card!
   You will need to have a FAT formatted Micro SD with your two 24bit BMP image files that are no larger than
   the display dimensions of 128x160px. You can create and export these images easily using the free image 
   manipullation software GIMP or your program of choice. Once you have your images name them img1.bmp and 
   img2.bmp then transfer them to your Micro SD card. After that it's just a matter of flashing the sketch to
   your 328PTFT board and inserting your SD card! 

   Added bonus: because pushing a full color image to the display is a rather resource heavy task for the tiny
   microcontroller there is a visible line by line wipe as the new image is being displayed, which works nicely
   as an unintentional transition effect :P

   To avoid having to remember internal connections of the board specific pins like digital pins 8, 9 and 10(which are 
   connected to the display's Reset, DC and CS pins) have been defined in the pin map file included with the custom board
   package for the 328PTFT v3. This means you can use TFT_BL for the backlight pin, SD_CS for the chip select pin of the 
   micro SD card, TFT_CS for the display's chip select pin, and so on, without having to remember the specific pin numbers.
   For a complete list of custom pin definitions check out the documentation provided at gitlab.com/MakerSpec/328PTFT
*/
#include <SD.h>
#include <TFT.h>
#include <SPI.h>

long lastTime = 0;//this will be used to change the image after a period of time
bool imgFlag = 0;//this variable will hold either a 1 or 0 to tell our program 
//which image to display. You could easily use a var type that can store more 
//bits of data if you wanted to alternate between more images.

//setup our integrated TFT display and call it "tft"
TFT tft = TFT(TFT_CS, TFT_DC, TFT_RST);

PImage img1;
PImage img2;

void setup() {
  pinMode(TFT_BL, OUTPUT);//set the backlight pin as an output
  tft.begin();//initialize the TFT
  tft.setRotation(3);//rotate the display so it is properly aligned for our board
  SD.begin(SD_CS); //pin 14(A0) is defined in the board pins file as SD_CS for ease of use
  tft.background(BLACK);
  img1 = tft.loadImage("IMG1.BMP");//load our images from our micro SD to be displayed
  img2 = tft.loadImage("IMG2.BMP");
  if (img1.isValid()) { //display the image or an error if the image is corrupt or cant be found
    tft.image(img1, 0, 0);
  } else {
    tft.stroke(EPURP);
    tft.setTextSize(2);
    tft.text("Image Error", 30, 55);
  }
  analogWrite(TFT_BL, 155);//turn on the backlight once the image is displayed
  lastTime = millis(); //save current time to start our 15 second countdown
}

void loop() {
  if (millis() - lastTime > 15000) {//once 15 seconds has passed
    if (imgFlag == 0) { //if imgFlag is false display the second image
      tft.image(img2, 0, 0);
    } else { //or if it's true display the first image again
      tft.image(img1, 0, 0);

    }
    lastTime = millis(); //overwrite the time to restart the countdown
    imgFlag = !imgFlag; //and alternate the imgFlag so next time around we display the opposite image.
  }
}
